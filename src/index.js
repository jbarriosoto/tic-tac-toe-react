import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import './index.css';


const Square = (props) => {
    return (
        <button className="square" 
        onClick={props.onClick}>
          {props.value}
        </button>
    );
}
 
//Convertimos componente de clase a componente funcional con Hooks 
const Board = ({squares, onClick}) =>   (

  <div className="gameShow">
        {squares.map((square, i) => (
            <Square key={i} value={square} onClick={() => onClick(i)} />
        ))}
    </div>
)

  const Game = () =>  {

    const [history, setHistory] = useState([Array(9).fill(null)]);
    const [stepNumber, setStepNumber] = useState(0);
    const [xIsNext, setXIsNext] = useState(true);
    const winner = calculateWinner(history[stepNumber]);

    const handleClick = i => {
      const timeInHistory = history.slice(0, stepNumber +1);
      const current = timeInHistory[stepNumber];
      const squares = [...current];
      
      //Si usuario hace click en un cuadrado ya ocupado o ya hay un ganador definido
      if (winner || squares[i]) return;
      //Renderiza una 'X' o un 'O' en el cuadrado clicado
      squares[i] = xIsNext ? 'X' : 'O';
      setHistory([...timeInHistory, squares]);
      setStepNumber(timeInHistory.length);
      setXIsNext(!xIsNext);
  }

    const jumpTo = step => {
      setStepNumber(step);
      setXIsNext(step % 2 === 0)
    };

    const renderMoves = () =>  (
      history.map((_step, move) => {
        const destination = move ? `Go to move#${move}` : 'Go to start' ;
        return (
          <li key={move}>
                <button onClick={() => jumpTo(move)}>{destination}</button>
          </li>

        )
      }) 
  )

    return (
      <div className="game">       
        <div className="game-board">
        <Board squares={history[stepNumber]} onClick={handleClick} />
        </div>
          
          <div className="game-info">
          <p>{winner ? 'Winner: ' + winner : 'Next Player: ' + (xIsNext ? 'X' : 'O')} </p>
          {renderMoves()}
          </div>
          </div>     
    )
  }
  //funcion predefinida para averiguar quien es el ganador
  function calculateWinner(squares) {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
        return squares[a];
      }
    }
    return null;
  }

  // ========================================
  

  ReactDOM.render(
    <Game />,
    document.getElementById('root')
  );
  